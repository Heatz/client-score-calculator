"""
Client Score Calculator

In order to get the Client Score you need to add the
Weight (invoices), Weight (delay), Weight (block price),
Weight (easy-ness) and divide the sum by 100.

To get the Weight (invoices):
    Invoices Weight Value = Invoice / MAX value in InvoiceList * Invoices Weight


To get the Weight (delay):
    Delay Weight Value = ( MAX value in AveDaysDelayList - AveDaysDelayValue) / MAX value in AveDaysDelayList * DelayWeight


To get the Weight (block price):
    BlockPriceWeightValue = BlockPriceOfClientValue / MAX value in BlockPriceOfClientList * BlockPriceWeight


To get the Weight (easy-ness):
    EasynessWorkingWithClientWeightValue = EasynessWorkingWithClientValue / MAX value in easyness_working_with_client_list * EasynessWeight

"""

import decimal

class ClientScoreCalculator():

    def __init__(self):
        self._invoices_weight = 0.10
        self._delay_weight = 0.20
        self._block_price_weight = 0.40
        self._easyness_weight = 0.30
    
    #Invoices Weight
    @property
    def invoices_weight(self):
        return self._invoices_weight

    @invoices_weight.setter
    def invoices_weight(self, value):
        self._invoices_weight = value

    #Delay Weight
    @property
    def delay_weight(self):
        return self._delay_weight

    @delay_weight.setter
    def delay_weight(self, value):
        self._delay_weight = value

    #Block Price Weight
    @property
    def block_price_weight(self):
        return self._block_price_weight
        
    @block_price_weight.setter
    def block_price_weight(self, value):
        self._block_price_weight = value

    #Easyness Weight
    @property
    def easyness_weight(self):
        return self._easyness_weight

    @easyness_weight.setter
    def easyness_weight(self, value):
        self._easyness_weight = value

    
    #invoice_weight_value Weight
    @property
    def invoices_weight_value(self):
        return self._invoices_weight_value

    @invoices_weight_value.setter
    def invoices_weight_value(self, value):
        self._invoices_weight_value = value

    # Delay Weight Value
    @property
    def delay_weight_value(self):
        return self._delay_weight_value

    @delay_weight_value.setter
    def delay_weight_value(self, value):
        self._delay_weight_value = value

    # Block Price of client Value
    @property
    def block_price_weight_value(self):
        return self._block_price_weight_value

    @block_price_weight_value.setter
    def block_price_weight_value(self, value):
        self._block_price_weight_value = value

    # Easyness Weight Value
    @property
    def easyness_weight_value(self):
        return self._easyness_weight_value

    @easyness_weight_value.setter
    def easyness_weight_value(self, value):
        self._easyness_weight_value = value

    
    def calc_invoice_weight_value(self, invoice_list, invoice):
        """Calculates the Invoice Weight Value 

        Formula:
            Invoices Weight Value = Invoice / MAX(InvoiceList) * Invoices Weight

        Arguments:
            
            invoice_list {[List]} -- [Number List of All Invoices]
            invoice {[Number]} -- [The Input Invoice Value of the Contact]
        """
        max_invoice_value = decimal.Decimal(max(invoice_list))
        self.invoices_weight_value = decimal.Decimal(invoice) / max_invoice_value  * decimal.Decimal(self.invoices_weight)
    
    def calc_delay_weight_value(self, ave_days_delay_list, ave_days_delay_value):
        """Calculates the Delay Weight Value

        Formula:
            Delay Weight Value = (MAX(AveDaysDelayList) - AveDaysDelayValue) / MAX(AveDaysDelayList) * DelayWeight

        
        Arguments:
            ave_days_delay_list {[List]} -- [Number List of All AverageDaysDelay]
            ave_days_delay_value {[Number]} -- [The Input Average days delayed of the Contact]
        """

        max_ave_days_delay = decimal.Decimal(max(ave_days_delay_list))
        self.delay_weight_value = (max_ave_days_delay - decimal.Decimal(ave_days_delay_value)) / max_ave_days_delay * decimal.Decimal(self.delay_weight)

    def calc_block_price_weight_value(self, block_price_of_client_list, block_price_of_client_value):
        """Calculates the Block Price Weight Value

        Formula:
            BlockPriceWeightValue = BlockPriceOfClientValue / MAX(BlockPriceOfClientList) * BlockPriceWeight
        
        Arguments:
            block_price_of_client_list {[List]} -- [Number List of All Block Price of Client]
            block_price_of_client_value {[Number]} -- [The Input Block Price of Client]
        """

        max_block_price_of_client = decimal.Decimal(max(block_price_of_client_list))
        self.block_price_weight_value = decimal.Decimal(block_price_of_client_value) / max_block_price_of_client  * decimal.Decimal(self.block_price_weight)

    def calc_easyness_weight_value(self, easyness_working_with_client_list, easyness_working_with_client_value):
        """Calculates the Easyness Weight Value

        Formula:
            EasynessWorkingWithClientWeightValue = EasynessWorkingWithClientValue / MAX(easyness_working_with_client_list) * EasynessWeight

        
        Arguments:
            easyness_working_with_client_list {[List]} -- [Number List of All Easyness Working with Client (Range of 1 - 10, 10 as the highest)]
            easyness_working_with_client_value {[Number]} -- [The Input Easyness Working with Client Value (Range of 1 - 10, 10 as the highest)]
        """

        max_easyness_working_with_client = decimal.Decimal(max(easyness_working_with_client_list))
        self.easyness_weight_value = decimal.Decimal(easyness_working_with_client_value) / max_easyness_working_with_client * decimal.Decimal(self.easyness_weight)
        
    def get_client_score(self):
        """Calculates and Returns the Client Score
        
        Formula:
            ClientScore = Weight (invoices) + Weight (delay) + Weight (block price) + Weight (easy-ness) / 100

        Returns:
            [Decimal] -- [Client Score]
        """

        client_score = (self.invoices_weight_value + self.delay_weight_value + self.block_price_weight_value + self.easyness_weight_value) * 100
        return client_score