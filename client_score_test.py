import unittest
from client_score import ClientScoreCalculator

invoices2018 = [
        1227888.87,
        250000.00,
        312163.93,
        125698.33,
        1091843.01,
        5039.18,
        260401.00,
        2690183.19,
        122500.00,
        2739135.35,
        220996.95,
        582907.50,
        2839585.76,
        375069.00,
        649286.60,
        580968.23,
        120985.99,
        20800.00,
        2500.00,
        0.00,
]

ave_days_delay_list = [
    5,
    5,
    18,
    2,
    5,
    13,
    38,
    23,
    1,
    47,
    10,
    98,
    118,
    86,
    10,
    25,
    5,
    1,
    314
]

block_price_of_client_list = [
    150.00,
    150.00,
    135.00,
    111.92,
    90.00,
    111.92,
    124.00,
    78.50,
    70.00,
    65.00,
    49.00,
    36.00,
    41.67,
    33.00,
]

easyness_working_with_client_list = [
    8,
    9,
    10,
    10,
    10,
    9,
    8,
    6,
    9,
    7,
    4,
    6,
    1,
    1,
]

class BlockCalculatorTest1Methods(unittest.TestCase):
    def test_invoice_weight_value(self):
        client_score_calculator = ClientScoreCalculator()
        client_score_calculator.calc_invoice_weight_value( invoice_list=invoices2018, invoice=1227888.86511345)
        self.assertEqual(round(client_score_calculator.invoices_weight_value, 2), 0.04)

    def test_delay_weight_value(self):
        client_score_calculator = ClientScoreCalculator()
        client_score_calculator.calc_delay_weight_value(ave_days_delay_list=ave_days_delay_list, ave_days_delay_value=5)
        self.assertEqual(round(client_score_calculator.delay_weight_value, 2), 0.20)

    def test_block_price_weight_value(self):
        client_score_calculator = ClientScoreCalculator()
        client_score_calculator.calc_block_price_weight_value(block_price_of_client_list=block_price_of_client_list, block_price_of_client_value=150)
        self.assertEqual(round(client_score_calculator.block_price_weight_value, 2), 0.40)

    def test_easyness_weight_value(self):
        client_score_calculator = ClientScoreCalculator()
        client_score_calculator.calc_easyness_weight_value(easyness_working_with_client_list=easyness_working_with_client_list, easyness_working_with_client_value=8)
        self.assertEqual(round(client_score_calculator.easyness_weight_value, 2), 0.24)

    def test_get_client_score(self):
        client_score_calculator = ClientScoreCalculator()
        client_score_calculator.calc_invoice_weight_value( invoice_list=invoices2018, invoice=1227888.86511345)
        client_score_calculator.calc_delay_weight_value(ave_days_delay_list=ave_days_delay_list, ave_days_delay_value=5)
        client_score_calculator.calc_block_price_weight_value(block_price_of_client_list=block_price_of_client_list, block_price_of_client_value=150)
        client_score_calculator.calc_easyness_weight_value(easyness_working_with_client_list=easyness_working_with_client_list, easyness_working_with_client_value=8)
        self.assertEqual(round(client_score_calculator.get_client_score(), 1), 88)

class BlockCalculatorTest2Methods(unittest.TestCase):
    def test_invoice_weight_value(self):
        client_score_calculator = ClientScoreCalculator()
        client_score_calculator.calc_invoice_weight_value( invoice_list=invoices2018, invoice=312163.93)
        self.assertEqual(round(client_score_calculator.invoices_weight_value, 2), 0.01)

    def test_delay_weight_value(self):
        client_score_calculator = ClientScoreCalculator()
        client_score_calculator.calc_delay_weight_value(ave_days_delay_list=ave_days_delay_list, ave_days_delay_value=18)
        self.assertEqual(round(client_score_calculator.delay_weight_value, 2), 0.19)

    def test_block_price_weight_value(self):
        client_score_calculator = ClientScoreCalculator()
        client_score_calculator.calc_block_price_weight_value(block_price_of_client_list=block_price_of_client_list, block_price_of_client_value=135.00)
        self.assertEqual(round(client_score_calculator.block_price_weight_value, 2), 0.36)

    def test_easyness_weight_value(self):
        client_score_calculator = ClientScoreCalculator()
        client_score_calculator.calc_easyness_weight_value(easyness_working_with_client_list=easyness_working_with_client_list, easyness_working_with_client_value=10)
        self.assertEqual(round(client_score_calculator.easyness_weight_value, 2), 0.30)

    def test_get_client_score(self):
        client_score_calculator = ClientScoreCalculator()
        client_score_calculator.calc_invoice_weight_value( invoice_list=invoices2018, invoice=312163.93)
        client_score_calculator.calc_delay_weight_value(ave_days_delay_list=ave_days_delay_list, ave_days_delay_value=18)
        client_score_calculator.calc_block_price_weight_value(block_price_of_client_list=block_price_of_client_list, block_price_of_client_value=135.00)
        client_score_calculator.calc_easyness_weight_value(easyness_working_with_client_list=easyness_working_with_client_list, easyness_working_with_client_value=10)
        self.assertEqual(round(client_score_calculator.get_client_score(), 1), 86)

if __name__ == '__main__':
    unittest.main()